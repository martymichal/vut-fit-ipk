// Copyright (C) 2021 Ondřej Míchal

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _DEFAULT_SOURCE
#define _GNU_SOURCE

#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <poll.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define FILENAME_LEN 128

static const char *login = "xmicha80";

static const char *program_name = "";

static const char *tmpfile_name = ".fileget_tmp";

char *nsaddress = NULL;
int nsport;

char *file_server_addr = NULL;
char file_server_ip[32] = "";
int file_server_port;

char *file_path = NULL;
char *file_name = NULL;

static bool verbose = false;

int connect_to_fileserver() {
    struct sockaddr_in fs_addr;
    int sock;

    fs_addr.sin_family = AF_INET;
    fs_addr.sin_port = htons(file_server_port);
    fs_addr.sin_addr.s_addr = inet_addr(file_server_ip);

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        return -1;
    }

    if (connect(sock, (struct sockaddr *) &fs_addr, sizeof(fs_addr)) == -1) {
        return -1;
    }

    return sock;
}

bool get_file(int *sock, char *filename, bool persistent) {
    int poll_rc;
    size_t dir_name_len;
    ssize_t bytes_recv;
    bool found_header = false;
    char msg[256] = "", res[BUFSIZ],
         *res_prot = NULL, *res_type = NULL, *res_cont = NULL,
         *dir_name = NULL, *dir_name_part = NULL,
         *filename_tmp = NULL;
    FILE *tmpf;
    struct pollfd fds;
    struct timespec timeout;

    fds.fd = *sock;
    fds.events = POLLIN | POLLOUT | POLLERR | POLLHUP | POLLNVAL;

    timeout.tv_sec = 2;
    timeout.tv_nsec = 0;

    if (snprintf(msg, 255, "GET %s FSP/1.0\r\nHostname: %s\r\nAgent: %s\r\n\r\n",
                 filename, file_server_addr, login) < 0) {
        warn("failed to assemble message to fileserver");
        return false;
    }

    tmpf = fopen(tmpfile_name, "w");
    if (tmpf == NULL) {
        // TODO: Handle memory
        warn("failed to open tmp file for writing");
        return false;
    }

get_file_send:
    poll_rc = ppoll(&fds, 1, &timeout, NULL);
    if (poll_rc == -1) {
        warn("failed to poll for events on tcp socket");
        return false;
    }

    if (poll_rc == 0) {
        *sock = connect_to_fileserver();
        if (*sock == -1) {
            warn("failed to reconnecct to fileserver");
        }
        goto get_file_send;
    }

    if (!(fds.revents & POLLOUT)) {
        goto get_file_send;
    }

    if (send(*sock, msg, sizeof(msg), MSG_NOSIGNAL) == 1) {
        warn("failed to send message to fileserver");
        return false;
    }

    while (1) {
        poll_rc = ppoll(&fds, 1, &timeout, NULL);
        if (poll_rc == -1) {
            warn("failed to poll for events on tcp socket");
            return false;
        }

        if (poll_rc == 0) {
            *sock = connect_to_fileserver();
            if (*sock == -1) {
                warn("failed to reconnect to fileserver");
                return false;
            }
            goto get_file_send;
        }

        if (fds.revents & POLLIN) {
            bytes_recv = recv(*sock, res, sizeof(res), 0);
            if (bytes_recv == -1) {
                warn("failed to receive message");
                return false;
            }

            if (bytes_recv == 0) {
                break;
            }

            // Process header of the response
            if (!found_header) {
                if (sscanf(res, "%ms %m[a-zA-Z ]", &res_prot, &res_type) == EOF) {
                    warn("failed to scan response from fileserver");
                    return false;
                }

                // TODO: Handle memory

                // Check protocol
                if (strcmp("FSP/1.0", res_prot) != 0) {
                    warnx("invalid protocol in response");
                    return false;

                }
                // Check response
                if (strcmp("Success", res_type) != 0) {
                    if (strcmp("Not Found", res_type) == 0) {
                        warnx("file %s is not on fileserver", filename);
                    } else {
                        warnx("unknown response from fileserver");
                    }

                    return false;
                }

                // Get rid of header in the response
                for (int i = 0; i < 3; i++) {
                    if (i == 0)
                        strtok(res, "\n");
                    else
                        strtok(NULL, "\n");
                }
                res_cont = strtok(NULL, "");

                if (res_cont != NULL) {
                    if (fwrite(res_cont, sizeof(char), strlen(res_cont), tmpf) != strlen(res_cont)) {
                        warn("failed to write to tmp file");
                    }
                }

                if (res_prot != NULL)
                    free(res_prot);
                if (res_type != NULL)
                    free(res_type);

                found_header = true;
                continue;
            }

            // It is safe to cast because bytes_recv holds -1 only on error
            if (fwrite(res, sizeof(char), bytes_recv, tmpf) != (size_t) bytes_recv) {
                warn("failed to write to tmp file");
            }
        }
    }
    fclose(tmpf);

    if (persistent) {
        filename_tmp = strdup(filename);

        dir_name = dirname(filename_tmp);
        dir_name_len = strlen(dir_name);
        if (strncmp(dir_name, ".", 1) != 0) {
            dir_name_part = strtok(dir_name, "/");
            dir_name = calloc(dir_name_len + 2, sizeof(char));
            if (dir_name == NULL) {
                warn("internal error");
                return false;
            }
            do {
                strcat(dir_name, dir_name_part);
                strcat(dir_name, "/");
                if (mkdir(dir_name, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) == -1 && errno != EEXIST) {
                    warn("failed to recreate directories as on server");
                    return false;
                }
            } while ((dir_name_part = strtok(NULL, "/")) != NULL);
            free (dir_name);
        }
        free(filename_tmp);

        if (rename(tmpfile_name, filename) != 0) {
            warn("failed to finalize file %s", filename);
            return false;
        }
    }

    return true;
}

char **update_file_list(char **file_list, int *num_of_files) {
    int max_num_of_files, nread;
    char *line, *line_ptr;
    size_t line_len = FILENAME_LEN;
    FILE *f;

    max_num_of_files = *num_of_files;

    // Read file -> get number of lines and length of the longest line
    f = fopen(tmpfile_name, "r");
    if (f == NULL) {
        return NULL;
    }

    line = malloc(line_len);
    if (line == NULL) {
        return NULL;
    }

    while ((nread = getline(&line, &line_len, f)) != -1) {
        if (line[nread-2] == '\r') {
            line[nread-2] = '\0';
        }

        if (*num_of_files == max_num_of_files) {
            if (max_num_of_files == 0) {
                max_num_of_files = 8;
            } else {
                max_num_of_files *= 2;
            }
            file_list = realloc(file_list, max_num_of_files * FILENAME_LEN);
            if (file_list == NULL) {
                return NULL;
            }
        }

        line_ptr = malloc(strlen(line) + 1);
        if (line_ptr == NULL) {
            return NULL;
        }
        strcpy(line_ptr, line);
        file_list[*num_of_files] = line_ptr;
        (*num_of_files)++;
    }
    
    free(line);
    fclose(f);

    if (unlink(tmpfile_name) == -1) {
        return NULL;
    }

    return file_list;
}

bool get_files() {
    int sock, num_of_files = 0;
    char **file_list;
    
    file_list = calloc(1, FILENAME_LEN);
    if (file_list == NULL) {
        warn("failed to prepare file list");
        return false;
    }

    sock = connect_to_fileserver();
    if (sock == -1) {
        warn("failed to connect to fileserver");
        return false;
    }
    
    // Download index file and all files listed in the index
    if (strcmp(file_name, "*") == 0) {
        if (!get_file(&sock, "index", false)) {
            return false;
        }

        file_list = update_file_list(file_list, &num_of_files);
        if (file_list == NULL) {
            warn("failed to update file list");
            return false;
        }
    } else {
        file_list[0] = file_path;
        num_of_files++;
    }

    for (int i = 0; i < num_of_files; i++) {
        sock = connect_to_fileserver();
        get_file(&sock, file_list[i], true);
        if (strcmp(file_name, "*") == 0) {
            free(file_list[i]);
        }
    }

    shutdown(sock, SHUT_RDWR);
    close(sock);

    free(file_list);
    return true;
}

bool resolve_address() {
    int sock, bytes, poll_rc;
    char msg[512] = "WHEREIS ", res[BUFSIZ],
    *res_type = NULL, *res_msg = NULL, *ip = NULL;
    struct sockaddr_in ns_addr;
    struct pollfd fds;
    struct timespec timeout;

    ns_addr.sin_family = AF_INET;
    ns_addr.sin_port = htons(nsport);
    ns_addr.sin_addr.s_addr = inet_addr(nsaddress);

    fds.events = POLLIN | POLLERR | POLLHUP | POLLNVAL;

    timeout.tv_sec = 1;
    timeout.tv_nsec = 0;

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock == -1) {
        warn("failed to create socket");
        return false;
    }

    fds.fd = sock;

    if (connect(sock, (struct sockaddr *) &ns_addr, sizeof(ns_addr)) == -1) {
        warn("failed to connect to nameserver");
        return false;
    }

    strcat(msg, file_server_addr);
    bytes = send(sock, msg, sizeof(msg), 0);
    if (bytes == -1) {
        warn("failed to send message");
        return false;
    }

    poll_rc = ppoll(&fds, 1, &timeout, NULL);
    if (poll_rc == -1) {
        warn("failed to poll nameserver socket");
        return false;
    }

    if (poll_rc == 0) {
        warnx("communication with nameserver timed out");
        return false;
    }

    if (fds.revents & (POLLERR | POLLHUP | POLLNVAL)) {
        warn("there was a problem with nameserver communication");
        return false;
    }

    if (!(fds.revents & POLLIN)) {
        warn("nameserver did not provide readable data");
        return false;
    }

    bytes = recvfrom(sock, res, sizeof(res), 0, NULL, NULL);
    if (bytes == -1) {
        warn("failed to receive message");
        return false;
    }

    res_type = strtok(res, " ");
    res_msg = strtok(NULL, "");

    if (strcmp(res_type, "ERR") == 0) {
        if (strcmp(res_msg, "Syntax") == 0) {
            warnx("internal error when sending message to nameserver");
            return false;
        }

        if (strcmp(res_msg, "Not Found") == 0) {
            warnx("unknown domain");
            return false;
        }
    }

    ip = strtok(res_msg, ":");
    strcpy(file_server_ip, ip);
    file_server_port = atoi(strtok(NULL, ""));

    shutdown(sock, SHUT_RDWR);
    close(sock);

    return true;
}

void parse_args(int argc, char *argv[]) {
    int option;
    char *nsport_str = NULL, *endptr;
    bool has_address = false, has_url = false;

    while ((option = getopt(argc, argv, "n:f:v")) != -1) {
        switch (option) {
        case 'n':
            nsaddress = strtok(optarg, ":");
            nsport_str = strtok(NULL, "");

            if (nsport_str == NULL) {
                errx(EXIT_FAILURE, "Nameserver address has to contain port");
            }

            errno = 0;
            nsport = strtol(nsport_str, &endptr, 10);
            if (*endptr != '\0' || errno != 0) {
                errx(EXIT_FAILURE, "Provided namespace port is not an integer");
            }
            
            has_address = true;
            break;
        case 'f':
            if (strncmp(optarg, "fsp://", 6) != 0) {
                errx(EXIT_FAILURE, "Unknown protocol. Supported protocol is \"fsp://\"");
            }

            file_server_addr = strtok(&optarg[6], "/");
            file_path = strtok(NULL, "");
            file_name = basename(file_path);

            if (file_path == NULL) {
                errx(EXIT_FAILURE, "Only file server name specified.");
            }

            has_url = true;
            break;
        case 'v':
            verbose = true;
            break;
        default:
            errx(EXIT_FAILURE, "Invalid option");
            break;
        }
    }

    if (!has_address) {
        errx(EXIT_FAILURE, "Missing nameserver. Use option -n");
    }

    if (!has_url) {
        errx(EXIT_FAILURE, "Missing file url. Use option -f");
    }
}

int main(int argc, char *argv[]) {
    program_name = argv[0];

    parse_args(argc, argv);

    if (!resolve_address()) {
        return EXIT_FAILURE;
    }

    if (!get_files()) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
